<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */

get_header(); ?>


	<main id="main" class="site-main" role="main">


		<div class="page-not-found post-header">
		    <div class="container">
			   <h1>Stranica nije pronađena!</h1>
			   <hr>
			</div>
		</div>
	

<?php
get_footer();
