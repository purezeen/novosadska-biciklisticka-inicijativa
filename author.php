<?php
/**
 * The template for displaying author posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); 

// get the current taxonomy term
$term = get_queried_object();
$image = get_field('hero_bg_image', $term);
?>

<!-- Hero section  -->
<section class="hero-section hero--small">
   <?php if ( $image ) { ?>
   <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $image['url']; ?>);">
   </div>
   <?php }elseif(have_rows( 'hero_section', 'option' )) { 
	while ( have_rows( 'hero_section', 'option' ) ) : the_row(); ?>
    <?php $default_hero_image = get_sub_field( 'default_hero_image' ); ?>
    <?php if( $default_hero_image ) { ?>
        <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $default_hero_image['url']; ?>);"></div>
    <?php }
    endwhile;
   } ?>
</section>
<!-- End hero section  -->

<main id="main" class="site-main" role="main">

<div class="main-content__wrap pt-5">
   <div class="container">
      <div class="sidebar-layout">
         <section class="main-content blog-page">

            <div class="blog-page-list">
            
               <?php if ( have_posts() ) : ?>

            <h1 class="mb-5"><?php echo get_queried_object() -> display_name; ?></h1>
               
               <?php while ( have_posts() ) : the_post(); ?>
                  <?php get_template_part( 'template-parts/content', 'page' ); ?>
               <?php endwhile; ?>
         
               <!-- Adding Previous and Next Post Links -->
               <div class="pagination">
                  <?php pagination_nav(); ?>
               </div>
               <?php endif; ?>
               <?php wp_reset_postdata(); ?>
            </div>
         </section>

         <?php get_sidebar(); ?>
      </div>
   </div>
</div>


<?php get_footer(); ?>
