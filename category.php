<?php
/**
 * The template for displaying posts by category.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); 

// get the current taxonomy term
$term = get_queried_object();

// vars
$image = get_field('hero_bg_image', $term);
?>

<?php
      $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
?>

<!-- Hero section  -->
<section class="hero-section hero--small">
   <?php if ( $image ) { ?>
   <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $image['url']; ?>);">
   </div>
   <?php }elseif(have_rows( 'hero_section', 'option' )) { 
	while ( have_rows( 'hero_section', 'option' ) ) : the_row(); ?>
    <?php $default_hero_image = get_sub_field( 'default_hero_image' ); ?>
    <?php if( $default_hero_image ) { ?>
        <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $default_hero_image['url']; ?>);"></div>
    <?php }
    endwhile;
   } ?>
</section>
<!-- End hero section  -->

<main id="main" class="site-main" role="main">
   <div class="main-content__wrap pt-4">
      <div class="container">
         <div class="sidebar-layout">
            <section class="main-content blog-page">
               <div class="blog-page-categories pl-0">
                  <ul class="unstyle-list">
                     <!-- Get blog categories  -->
                     <li><a href="<?php echo $my_home_url; ?>/blog/"><?php _e( 'Objave', 'nsbi' ); ?></a></li>
                     <?php
                    $categories = get_categories(); 
                     foreach ($categories as $category) {
                        if( $category->slug != 'objave') { ?>

                           <?php $class = ( is_category( $category->slug ) ) ? 'active-cat' : ''; ?>
                           <li class="<?php echo $class; ?>"><a
                                 href="<?php echo get_category_link( $category->term_id )?>"><?php echo $category->name; ?></a>
                           </li>
                        <?php } ?>
                     <?php }  ?>
                  </ul>

               </div>

               <div class="blog-page-list">
                  <div>
                     <?php
                        $currCat = get_category(get_query_var('cat'));
                        $cat_name = $currCat->name;
                        $cat_id   = get_cat_ID( $cat_name );
                        ?>

                     <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $temp = $wp_query;
                        $wp_query = null;
                        $wp_query = new WP_Query();
                        $wp_query->query('showposts=6&post_type=post&paged='.$paged.'&cat='.$cat_id);
                        while ($wp_query->have_posts()) : $wp_query->the_post();
                        ?>
                     <?php get_template_part( 'template-parts/content', 'page' ); ?>
                     <?php endwhile; ?>
                  </div>

                  <!-- Adding Previous and Next Post Links -->
                  <div class="pagination">
                     <?php pagination_nav(); ?>
                  </div>

                  <?php wp_reset_postdata(); ?>
               </div>
            </section>
            <?php get_sidebar(); ?>
         </div>
      </div>
   </div>


   <?php get_footer(); ?>