<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!-- Get home url  -->
<?php $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) ); ?>

      <!-- Float social icons  -->
      <?php if ( have_rows( 'social_links', 'option' ) ) : ?>
         <?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>
         <div class="social-icon__wrap">
            <div class="float-icon social-icon">

               <a class="social-icon-link" href="<?php the_sub_field( 'facebook' ); ?>" target="_blank" aria-label="Facebook" rel="noopener">
                  <div class="icon"><span class="iconify" data-icon="brandico:facebook" data-inline="false"></span></div>
                  <p class="social-icon-text">Facebook</p>
               </a>

               <?php if(get_sub_field( 'youtube' )) { ?>
                  <a class="social-icon-link" href="<?php the_sub_field( 'youtube' ); ?>" target="_blank" aria-label="Youtube" rel="noopener">
                     <div class="icon"><span class="iconify" data-icon="vaadin:youtube" data-inline="false"></span></div>
                     <p class="social-icon-text">Youtube</p>
                  </a>
               <?php
               } ?>

               <?php if(get_sub_field( 'twitter' )) { ?>
                  <a class="social-icon-link" href="<?php the_sub_field( 'twitter' ); ?>" target="_blank" aria-label="Twitter" rel="noopener">
                     <div class="icon"><span class="iconify" data-icon="ant-design:twitter-outlined" data-inline="false"></span>
                     </div>
                     <p class="social-icon-text">Twitter</p>
                  </a>
               <?php } ?>

               <?php if(get_sub_field( 'instagram' )) { ?>
                  <a class="social-icon-link" href="<?php the_sub_field( 'instagram' ); ?>" target="_blank" aria-label="Instagram" rel="noopener">
                     <div class="icon"><span class="iconify" data-icon="ant-design:instagram-outlined" data-inline="false"></span>
                     </div>
                     <p class="social-icon-text">Instagram</p>
                  </a>
               <?php } ?>
               <?php if(get_sub_field( 'linkedin' )) { ?>
                  <a class="social-icon-link" href="<?php the_sub_field( 'linkedin' ); ?>" target="_blank" aria-label="Linkedin" rel="noopener">
                     <div class="icon"><span class="iconify" data-icon="il:linkedin" data-inline="false"></span>
                     </div>
                     <p class="social-icon-text">LinkedIn</p>
                  </a>
               <?php } ?>
            </div>
         </div>
         <?php endwhile; ?>
      <?php endif; ?>
      <!-- End float social icons  -->

      <div class="donate-icon">
         <a href="/doniraj" class="donate-icon-wrap" >
            <img class="donate-icon-mobile" src="<?php echo get_template_directory_uri() ?>/img/donate-icon-mobile.svg"
               alt="doniraj" width="30" height="30">
            <span class="donate-icon-text"><?php _e('DONIRAJ', 'nsbi'); ?></span>
            <img src="<?php echo get_template_directory_uri() ?>/img/donate-icon-desktop.svg" alt="doniraj"
               class="donate-icon-desktop" width="100" height="100">
         </a>
         <a href="#"></a>
      </div>
   </main>

   <!-- Counter section  -->
   <?php get_template_part( 'template-parts/acf/cta', 'donate' ) ?>
   <!-- End counter section  -->

   <footer id="colophon" class="site-footer background-grey section-small" role="contentinfo">
      <div class="container">
         <div class="row">
            <div class="col-6 col-md-6 col-lg-3 pb-5 pb-lg-0 footer-logo">
               <?php $footer_logo = get_field( 'footer_logo', 'option' ); ?>
               <?php if ( $footer_logo ) { ?>
               <a href="<?php echo $my_home_url; ?>" class="navbar-brand d-block">
                  <img src="<?php echo $footer_logo['url']; ?>" alt="<?php echo $footer_logo['alt']; ?>" width="192"
                     height="62">
               </a>
               <?php } ?>
               <p class="copyright">Copyright
                  <? echo date('Y');?> <?php _e( 'Novosadska biciklisticka inicijativa. Sva Prava Zadržana.', 'nsbi' ); ?>
               </p>
            </div>
            <div class="col-6 col-md-6 col-lg-3 pb-5 pb-lg-0">
               <h4><?php _e( 'Nagrade', 'nsbi' ); ?></h4>

               <?php if ( have_rows( 'awards', 'option' ) ) : ?>
                  <?php while ( have_rows( 'awards', 'option' ) ) : the_row(); ?>
                  <p class="list-icon"><span class="iconify" data-icon="la:award-solid" data-inline="false"></span>
                     <?php the_sub_field( 'text' ); ?></p>
                  <?php endwhile; ?>
               <?php endif; ?>
            </div>

            <div class="col-6 col-md-6 col-lg-3 pb-5 pb-lg-0">

               <?php if ( have_rows( 'contact_info', 'option' ) ) : ?>
                  <?php while ( have_rows( 'contact_info', 'option' ) ) : the_row(); ?>
                     <?php the_sub_field( 'text' ); ?>
                     <?php if ( have_rows( 'location' ) ) : ?>
                        <?php while ( have_rows( 'location' ) ) : the_row(); ?>
                        <a href="<?php the_sub_field( 'google_map_link' ); ?>" target="_blank" rel="noopener">
                           <p class="list-icon"><span class="iconify" data-icon="cil:location-pin"
                                 data-inline="false"></span><?php the_sub_field( 'street_address' ); ?></p>
                        </a>
                        <?php endwhile; ?>
                     <?php endif; ?>

                     <a href="emailto:<?php the_sub_field( 'email' ); ?>">
                        <p class="list-icon"><span class="iconify" data-icon="la:envelope-solid"
                              data-inline="false"></span><?php the_sub_field( 'email' ); ?></p>
                     </a>
                  <?php endwhile; ?>
               <?php endif; ?>
            </div>
            <div class="col-6 col-md-6 col-lg-3 pb-5 pb-lg-0">
               <h4><?php _e('Imate pitanja?', 'nsbi'); ?></h4>
               <p><?php _e('Javite nam se, biće nam drago da vas čujemo!', 'nsbi'); ?></p>
               <a href="<?php echo $my_home_url; ?><?php _e('/kontakt/', 'nsbi'); ?>" class="btn btn-border-green mt-2"><?php _e('Kontakt', 'nsbi'); ?></a>
            </div>
         </div>
      </div>
   </footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>