<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'gulp-wordpress' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-lightbox' );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );


/**
 *  Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';


/**
 * Acf gutemberg blocks
 */
require get_template_directory() . '/inc/acf-blocks.php';


add_post_type_support( 'page', 'excerpt' );

// Register Custom Post Type
function cpt_team() {

	$labels = array(
		'name'                  => _x( 'Team', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Team', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Team', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Team', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Team', 'gulp_wordpress' ),
		'description'           => __( 'Team  Description', 'gulp_wordpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'main titile' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest' => true,
		'menu_icon'   => 'dashicons-format-image',
	);
	register_post_type( 'Team', $args );

}

add_action( 'init', 'cpt_team', 0 );


//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options acf',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact',
		'menu_title'	=> 'Contact',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

}



add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Main color', 'genesis-sample' ),
		'slug'  => 'main-color',
		'color'	=> '#85C226',
	),
	array(
		'name'  => __( 'Second color', 'genesis-sample' ),
		'slug'  => 'second-color',
		'color' => '#209350',
	),
	array(
		'name'  => __( 'Dark green', 'genesis-sample' ),
		'slug'  => 'dark-green',
		'color' => '#296A45',
		 ),
	array(
	'name'  => __( 'Background color', 'genesis-sample' ),
	'slug'  => 'background-color',
	'color' => '#EFEFEF',
		),
	array(
		'name'  => __( 'Grey', 'genesis-sample' ),
		'slug'  => 'grey',
		'color' => '#989898',
			),
	array(
		'name'  => __( 'Light grey', 'genesis-sample' ),
		'slug'  => 'light grey',
		'color' => '#EFEFEF',
			),
) );


// Adding Previous and Next Post Links
function pagination_nav() {
	global $wp_query;

	$total = $wp_query->max_num_pages;

	if ($total > 1) {
		// get the current page
		if (!$current_page = get_query_var('paged')) {
				$current_page = 1;
		}
		?>

	<div class="center">
		<ul class="unstyle-list">
			<li class="prev">
				<?php previous_posts_link('<img src="'.get_template_directory_uri().'/img/slider-arrow-left.png" />'); ?>
			</li>
			<span class="pagination-number">
				<?php echo $current_page = ($current_page < 10) ? "0" . $current_page : $current_page; 
						echo "/"; 
						echo $total = ($total < 10) ? "0" . $total : $total; 
						?></span>
			<li class="next">
				<?php next_posts_link('<img src="'.get_template_directory_uri().'/img/slider-arrow-right.png" />'); ?>
			</li>
		</ul>
	</div>

	<?php
	}
}

// Modify the main loop on your category page 
function wpse_modify_category_query( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
		 if ( $query->is_category() ) {
			  $query->set( 'posts_per_page', 2 );
		 } 
	} 
}
add_action( 'pre_get_posts', 'wpse_modify_category_query' );

function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

// Category base name same as page name
function wpa_fix_blog_pagination(){
	add_rewrite_rule(
		 'blog/page/([0-9]+)/?$',
		 'index.php?pagename=blog&paged=$matches[1]',
		 'top'
	);
}
add_action( 'init', 'wpa_fix_blog_pagination' );