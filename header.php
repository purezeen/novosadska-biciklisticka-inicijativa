<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
   <meta charset="<?php bloginfo( 'charset' ); ?>">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="profile" href="http://gmpg.org/xfn/11">
   <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

   <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
   <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />

   <?php wp_head(); ?>

   <!-- Google fonts Montserrat, Quicksand -->
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 

   <!-- Iconify  -->
   <script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
</head>

<body <?php body_class(); ?>>

   <script type="text/javascript">
   var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
   </script>

   <div id="page" class="site">

      <header id="masthead" class="header background-dark" role="banner">
         <div class="container">
            <div class="row header-wrap">
               <?php
                  $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
               ?>
               <a href="<?php echo $my_home_url; ?>" class="site-branding">
                     <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.svg"
                     alt="Novosadska biciklisticka inicijativa" width="188" height="67">
               </a>
               <div id="nav-icon">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
               </div>
               <nav class="nav-wrap">
                  <div class="nav">
                     <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'navigation-menu', 'container' => '' ) ); ?>
                     <div class="right-menu">
                  
                        <?php 
                        get_search_form(); 
                        ?>
                        <?php do_action('wpml_add_language_selector'); ?>
                     
                     </div>
                  </div>
               </nav><!-- #site-navigation -->
            </div>
         </div>
      </header><!-- #masthead -->
      <!-- #masthead -->
