<?php /* Template Name: Home page old*/ 

get_header(); ?>


<!-- Hero section  -->
<section class="hero-section">
         <?php $hero_image = get_field( 'hero_image' ); ?>
         <?php if ( $hero_image ) { ?>
         <div class="hero-section__img cover top-gradient"
            style="background-image: url(<?php echo $hero_image['url']; ?>);">
            <div class="container text-center">
               <h1 class="hero-section__title"><?php the_field( 'hero_title' ); ?></h1>
            </div>
         </div>
         <?php } ?>
</section>
<!-- End hero section  -->


<main id="main" class="site-main">


   <!-- Two columns section -->
   <!-- <?php //if ( have_rows( 'two_columns' ) ) : ?>
   <section class="container section">

      <?php// while ( have_rows( 'two_columns' ) ) : the_row(); ?>

      <?php //if ( get_sub_field( 'column_reverse' ) == 1 ) { 
              // $column_reverse = "two-columns-reverse";
           // } else { 
              // $column_reverse = "";
           //} ?>

      <div class="row two-columns align-items-center <?php //echo $column_reverse; ?> ">
         <?php //$image = get_sub_field( 'image' ); ?>
         <?php //if ( $image ) { ?>
         <div class="col-12 col-md-6">
            <div class="circle-shapes mb-5 mb-lg-0">
               <div class="big-circle cover"
                  style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php //echo $image['sizes']['medium']; ?>)">
               </div>
            </div>
         </div>
         <?php //} ?>
         <?php //if ( have_rows( 'content' ) ) : ?>
         <?php //while ( have_rows( 'content' ) ) : the_row(); ?>
         <div class="col-12 col-md-6">
            <div class="line-left box-content">
               <h2 class="color-dark-green mb-normal"><?php //the_sub_field( 'title' ); ?></h2>
               <p><?php //the_sub_field( 'text' ); ?></p>
               <?php //$cta = get_sub_field( 'cta' ); ?>
               <?php //if ( $cta ) { ?>
               <a href="<?php //echo $cta['url']; ?>" target="<?php //echo $cta['target']; ?>"
                  class="btn btn-full btn-light-green btn-center"><?php //echo $cta['title']; ?></a>
               <?php //} ?>
            </div>
         </div>
         <?php //endwhile; ?>
         <?php //endif; ?>
      </div>
      <?php //endwhile; ?>
   </section>
   <?php //else : ?>
   <?php ?>
   <?php //endif; ?> -->
   <!-- End two columns section -->


   <!-- Counter section  -->
   <?php //get_template_part( 'template-parts/acf/counter' ) ?>
   <!-- End counter section  -->


   <!-- Slider section  -->
   <!-- <section class="services-slider section background-grey">

      <?php //echo get_the_excerpt(); ?>

      <?php 
         //query_posts("page_id=213");
         //while ( have_posts() ) : the_post()
      ?>
      <div class="row">
         <div class="col-12 d-flex justify-content-center">
            <h2 class="color-black mb-big"><?php //echo get_the_title(); ?></h2>
         </div>
      </div>
     
      <div class="container">
         <div class="row">
            <div class="center col-12">
             
               <div class="container background-white">
                  <div class="row justify-content-center">
                     <div class="col-12 col-sm-7">
                        <div class="services_content line-left">
                           <?php //the_excerpt(); ?>
                           <a href="<?php //echo the_permalink(); ?>" class="btn btn-full btn-light-green">Pročitaj više</a>
                        </div>
                     </div>
                     <div class="col-12 col-sm-5 services-image cover"
                        style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php //echo get_template_directory_uri()?>/img/o-nama.png)">
                     </div>
                  </div>
               </div>
            </div> -->

            <!-- <div class="paginator center text-color text-center">
               <ul class="unstyle-list">
                  <li class="prev"></li>
                  <span class="pagingInfo"></span>
                  <li class="next"></li>
               </ul>
            </div> -->
         <!-- </div>
      </div>
      
      <?php
         //endwhile; 
        // wp_reset_query();
      ?>      
   </section> -->
   <!-- End slider section  -->


    <!-- Get acf flexible section  -->
    <?php //get_template_part( 'template-parts/acf/flexible', 'section' ) ?>
   <!-- Endflexible section  -->


   <!-- Latest news  -->
   <!-- <section class="blog-page section">
      <div class="container">

         <div class="row">
            <div class="col-12 d-flex justify-content-center">
               <h2 class="mb-big color-dark-green">Poslednje vesti</h2>
            </div>
         </div>

         <div class="blog-page-list">
            <?php
           // $args = array(
          //     'post_type' => 'post',
          //     'posts_per_page' => 3,
          ///     'post_status' => 'publish'
         //   );

          //  $the_query = new WP_Query( $args );

           // if ($the_query->have_posts()) : ?>

               <div>
               //Get blog post block
                  <?php //while ($the_query->have_posts()) : $the_query->the_post(); ?>
         
                     <?php //get_template_part( 'template-parts/content-page' ) ?>
                  
                  <?php //endwhile; ?>
               </div>

               <?php 
           // endif; 
           // wp_reset_postdata();
            ?>
         </div>
         <div class="row justify-content-center">
            <div class="col-12 text-center">
               <a href="/kategorija/sve_vesti/" class="btn btn-full btn-light-green btn-center margin-large">Pogledaj sve vesti</a>
            </div>
         </div>
      </div>
   </section> -->
   <!-- End latest news  -->
<div class="container">
   <div class="sidebar-layout">
   <div class="main-content">

      <section class="featured-posts">
       
         <div class="row">
         <div class="col-md-6">
         <a href="" class="featured-post">
         <div class="featured-post__content">
            <div class="featured-post__info">
              <div class="featured-post__author">Autor: <strong>Milica Blagojevic</strong></div>
              <div class="featured-post__date"><strong>14.</strong> NOV</div>
            </div>
            <h2 class="featured-post__title">Holandija ulaže 100 miliona € da izvuče 200.000 ljudi iz automo...</h2>
            <p>Holandski državni sekretar za infrastrukturu, Stientje Van Veldhoven najavio je investiranje dodatnih 100 miliona eura u biciklistički saobraćaj. Ulaganje...</p>
            <div class="featured-post__btn"><span class="btn-link btn-link_green">Saznaj više</span></div>
           </div>
            <?php
               // if (has_post_thumbnail()) {
               //    $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
               //    $backgroundImg = $backgroundImg[0];
               // }else {
               //    $backgroundImg="";
               // }
            ?>
            <!-- <?php //echo $backgroundImg;?> -->
            <div class="post-item_image cover" style="background-image:url('http://nsbi1.local/wp-content/uploads/2020/09/blog-single-768x348.png')"></div>
            <div class="featured-post__banner">
            <div class="cat-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/cat-icon-white.png"><span>Srbija</span><span>Vesti</span>
            </div>
            <div class="tag-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tag-icon-white.png"><span class="tag-box__item">aktivnosti</span><span class="tag-box__item">finansije</span><span class="tag-box__item">bicikli</span>
            </div>

            <div class="comment-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/comment-white.png"><span class="comment-box__num">(8)</span>
            </div>
            </div>
           </a>     
         </div>

       
         <div class="col-md-6">
         <a href="" class="featured-post">
         <div class="featured-post__content">
            <div class="featured-post__info">
              <div class="featured-post__author">Autor: <strong>Milica Blagojevic</strong></div>
              <div class="featured-post__date"><strong>14.</strong> NOV</div>
            </div>
            <h2 class="featured-post__title">Holandija ulaže 100 miliona € da izvuče 200.000 ljudi iz automo...</h2>
            <p>Holandski državni sekretar za infrastrukturu, Stientje Van Veldhoven najavio je investiranje dodatnih 100 miliona eura u biciklistički saobraćaj. Ulaganje...</p>
            <div class="featured-post__btn"><span class="btn-link btn-link_green">Saznaj više</span></div>
           </div>
            <?php
               // if (has_post_thumbnail()) {
               //    $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
               //    $backgroundImg = $backgroundImg[0];
               // }else {
               //    $backgroundImg="";
               // }
            ?>
            <!-- <?php //echo $backgroundImg;?> -->
            <div class="post-item_image cover" style="background-image:url('http://nsbi1.local/wp-content/uploads/2020/09/blog-single-768x348.png')"></div>
            <div class="featured-post__banner">
            <div class="cat-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/cat-icon-white.png"><span>Srbija</span><span>Vesti</span>
            </div>
            <div class="tag-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tag-icon-white.png"><span class="tag-box__item">aktivnosti</span><span class="tag-box__item">bicikli</span><span class="tag-box__item">priroda</span><span class="tag-box__item">finansije</span><span class="tag-box__item">bicikli</span>
            </div>

            <div class="comment-box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/comment-white.png"><span class="comment-box__num">(8)</span>
            </div>
            </div>
           </a>  
         </div>
           </div>
      
      </section>
           
     

      <section class="latest-posts">
            <?php
            $args = array(
               'post_type' => 'post',
               'posts_per_page' => 3,
               'post_status' => 'publish'
           );

           $the_query = new WP_Query( $args );

            if ($the_query->have_posts()) : ?>

               <div>
               <!-- Get blog post block -->
                  <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
         
                     <?php get_template_part( 'template-parts/content-page' ) ?>
                  
                  <?php endwhile; ?>
               </div>

               <?php 
            endif; 
           wp_reset_postdata();
            ?>

           <div class="latest-posts__btn">
               <a href="/kategorija/sve_vesti/" class="btn btn-full btn-light-green btn-center margin-large">Pogledaj sve vesti</a>
          </div>
         </section>
        


     
   </div>

   <?php get_sidebar(); ?>
   </div>
   </div>


   <?php get_footer();?>