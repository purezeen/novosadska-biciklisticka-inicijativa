<?php /* Template Name: Home page */ 

get_header(); 

?>

<main id="main" class="site-main">

    <!-- Hero section  -->
    <?php if (has_post_thumbnail()) { ?>

        <section class="hero-section">
            <div class="hero-section__img cover top-gradient" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
        </section>

    <?php } ?>
    <!-- End hero section  -->

    <div class="main-content__wrap">
        <div class="container">
            <div class="sidebar-layout">
                <div class="main-content">

                    <section class="featured-posts">

                        <div class="row">

                            <?php $featured_posts = get_field( 'featured_posts' ); ?>
                            <?php if ( $featured_posts ):  ?>

                                <?php foreach ( $featured_posts as $post ):  ?>
                                <?php setup_postdata( $post ); ?>

                                <div class="col-md-6">
                                    <div class="featured-post">
                                        <div class="featured-post__content">
                                            
                                            <a class="featured-post__title" href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                                            
                                            <?php the_excerpt(); ?>

                                            <div class="featured-post__btn"><a href="<?php the_permalink(); ?>" class="btn-link btn-link_green btn-arrow"><?php _e( 'Saznaj više', 'nsbi' ); ?></a></div>
        
                                        </div>
                                        <?php
                                        if (has_post_thumbnail()) {
                                            $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
                                            $backgroundImg = $backgroundImg[0];
                                        }else {
                                            $backgroundImg="";
                                        }
                                        ?>
                                        <div class="aspect-ratio-bg cover"
                                            style="background-image:url(<?php echo $backgroundImg;?>)">
                                        </div>
                                        <div class="featured-post__banner">

                                            <?php $category_post = get_the_category(); 
                                            foreach ($category_post as $cat) {
                                            if ($cat->slug != 'objave') { ?>

                                                <a href="<?php echo get_category_link($cat->term_id)?>" class="cat-box">
                                                    <img
                                                        src="<?php echo get_template_directory_uri(); ?>/img/cat-icon-white.png" alt="icon" width="16" height="16"><span
                                                        class="cat-box__item"><?php echo $cat ->name; ?></span>
                                                </a>
                                            <?php }
                                            }  ?>

                                            <?php $tags_post = get_the_tags();
                                            if ( $tags_post ) : ?>
                                            <div class="tag-box"> <img src="<?php echo get_template_directory_uri(); ?>/img/tag-icon-white.png" alt="icon" width="20" height="20">
                                                <?php foreach ( $tags_post as $tag ) : ?>
                                                <a class="tag-box__item"
                                                    href="<?php echo get_tag_link($tag->term_id)?>"><?php echo $tag ->name; ?></a>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>

                                            <div class="featured-post__info">
                                                <div class="featured-post__author">Autor: <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                                                    <?php echo get_the_author_meta('user_firstname'); ?>
                                                    <?php echo get_the_author_meta('user_lastname'); ?></a></div>
                                                <div class="featured-post__date uppercase">
                                                    <?php $post_date = get_the_date( "d.m.Y.", get_the_ID() ); echo $post_date; ?>
                                                </div>
                                            </div>

                                        </div>

                                        <a class="card-link" href="<?php the_permalink(); ?>"></a>
                                    </div>
                                </div>

                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>

                            <?php endif; ?>
                        </div>

                    </section>

                    <section class="latest-posts">
                        <?php
                            $exclude_ids = wp_list_pluck( $featured_posts, 'ID' );
                            $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => 3,
                            'post_status' => 'publish',
                            'post__not_in' => $exclude_ids
                        );

                        $the_query = new WP_Query( $args );

                        if ($the_query->have_posts()) : ?>

                            <div>
                                <!-- Get blog post block -->
                                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                <?php get_template_part( 'template-parts/content-page' ) ?>

                                <?php endwhile; ?>
                            </div>

                        <?php 
                        endif; 
                        wp_reset_postdata();
                        ?>

                        <?php $add_button_for_view_all_news = get_field( 'add_button_for_view_all_news' ); ?>
                        <?php if ( $add_button_for_view_all_news ) { ?>

                            <div class="latest-posts__btn">
                                <a href="<?php echo $add_button_for_view_all_news['url']; ?>"
                                    class="btn btn-full btn-light-green btn-center margin-large" target="<?php echo $add_button_for_view_all_news['target']; ?>"><?php echo $add_button_for_view_all_news['title']; ?></a>
                            </div>
                            
                        <?php } ?>

                    </section>
                </div>

                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

    <?php get_footer();?>