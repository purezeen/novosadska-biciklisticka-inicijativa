<?php
/** Template Name: Blog template
 */

get_header(); 

// get the current taxonomy term
$term = get_queried_object();
?>

<?php
      $my_home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
?>

<!-- Hero section  -->
<?php if ( have_rows( 'hero_section', 'option' ) ) : ?>
	<?php while ( have_rows( 'hero_section', 'option' ) ) : the_row(); ?>

        <section class="hero-section hero--small">

            <?php $default_hero_image = get_sub_field( 'default_hero_image' ); ?>
            <?php $hero_image_objave = get_sub_field( 'hero_image_-_objave' ); ?>

            <?php if ( $hero_image_objave ) { ?>
               <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $hero_image_objave['url']; ?>);"></div>
            <?php } elseif( $default_hero_image ) { ?>
               <div class="hero-section__img cover top-gradient" style="background-image: url(<?php echo $default_hero_image['url']; ?>);"></div>
            <?php } ?>

        </section>

	<?php endwhile; ?>
<?php endif; ?>
<!-- End hero section  -->

<main id="main" class="site-main" role="main">

   <div class="main-content__wrap pt-4">
      <div class="container">
         <div class="sidebar-layout">

            <section class="main-content blog-page">
               <div class="blog-page-categories pl-0">
                  <ul class="unstyle-list">
                     <!-- Get blog categories -->
                     <li class="active-cat"><a href="<?php echo $my_home_url; ?>/blog/"><?php _e( 'Objave', 'nsbi' ); ?></a></li>
                     <?php
                        $categories = get_categories(); 

                        foreach ($categories as $category) { 
                           if( $category->slug != 'objave') { ?>
                              <li><a href="<?php echo get_category_link( $category->term_id )?>"><?php echo $category->name; ?></a>
                              </li>
                           <?php }
                     }  ?>
                  </ul>
               </div>

               <div class="blog-page-list">
                  <div>
                     <?php
                     $paged_number = (get_query_var('paged')) ? get_query_var('paged') : 1;
                     $query = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page'=> 6,
                        'paged' => $paged ) 
                        );

                     if ($query->have_posts()) :
                           while ($query->have_posts()) :
                           $query->the_post();
                           get_template_part( 'template-parts/content', 'page' );
                           endwhile;
                        endif;
                    ?>
                  </div>

                  <!-- Adding Previous and Next Post Links -->
                  <div class="pagination">
                     <?php pagination_nav(); ?>
                  </div>

                  <?php wp_reset_postdata(); ?>
               </div>

            </section>

            <?php get_sidebar(); ?>
         </div>
      </div>
   </div>

   <?php get_footer(); ?>