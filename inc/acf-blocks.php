<?php

/**
 *
 * ACF GUTEMBERG BLOCKS
 *
 */

// Register gutenberg blocks 
function register_acf_block_types() {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'Hightlight text block',
            'title'             => __('Hightlight text block'),
            'description'       => __('A custom Hightlight text block.'),
            'render_template'   => 'template-parts/blocks/common/hightlight-text-block.php',
            'category'          => 'layout',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'text', 'percentage' ),
            'mode' => 'edit',
        ));

        // register a sidebar block.
        acf_register_block_type( array(
            'name'				=> 'Sidebar promo card',
            'title'				=> __( 'Sidebar promo card' ),
            'render_template'		=> 'template-parts/blocks/widgets/sidebar-card.php',
            'category'			=> 'sections',
            'icon'				=> 'list-view',
            'mode'				=> 'edit',
        ));

           // register a sidebar block.
        acf_register_block_type( array(
            'name'				=> 'Image slider',
            'title'				=> __( 'Image slider' ),
            'render_template'		=> 'template-parts/blocks/image-slider/image-slider.php',
            'category'			=> 'sections',
            'icon'				=> 'list-view',
            'mode'				=> 'edit',
        ));
    
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}


// Local json 
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $path;
}
 