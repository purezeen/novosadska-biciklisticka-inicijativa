<?php

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {

	// Bootstrap css
	wp_enqueue_style( 'bootstrap-css',  get_template_directory_uri() . '/css/bootstrap.css' );

	// main css
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), '1.5467' );

	// Slick css
	wp_enqueue_style( 'slick-css',  get_template_directory_uri() . '/css/slick.css' );

	wp_enqueue_style( 'lightcase-css',  get_template_directory_uri() . '/css/lightcase.css' );

	
	// Waypoint 
	// wp_enqueue_script( 'waypoint', get_template_directory_uri() . '/js/noframework.waypoints.min.js', array(), '2', true );

	// Slick js
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '2', true );

	wp_enqueue_script( 'lightcase-js', get_template_directory_uri() . '/js/lightcase.js', array('jquery'), '2', true );

	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.js', array('jquery'), '2', true );

	// main js
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '2', true );	
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		// wp_enqueue_script( 'comment-reply' );
	}
	if ( is_page_template( 'template/contact-template.php' )) {
		// Mapbox js
	   wp_enqueue_script( 'map_box_js', 'https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js', array(), '2', true );
	
		// Mapbox js
		wp_enqueue_script( 'map_box_js_js', get_template_directory_uri() . '/js/map_box.js', array(), '2', true );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );