/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function($) {


    $(document).ready(function() {

        /*----------  Masonry  ----------*/
        setTimeout(function() {
            $('.grid').masonry({
                columnWidth: '.grid-sizer',
                    itemSelector: '.grid-item',
                    percentPosition: true,
                    gutter: 30
            });
        }, 1000);

        /*----------  Search input animation  ----------*/

        if ($('#inpt_search').val()) {
            $('.search-input').addClass('active');
        }

        // Input field - search expand 
        $("#inpt_search").on('focus', function() {
            $(this).parent('label').addClass('active');
        });

        $("#inpt_search").on('blur', function() {
            if ($(this).val().length == 0)
                $(this).parent('label').removeClass('active');
        });


        /*----------  Donate icon animation / first and second click  ----------*/

        if ($(document).width() < 992) {
            $('.donate-icon-wrap').click(function(e) {
                // First and second click 
                // Mobile version - donate icon 
                var clicks = $(this).data('clicks');
                if (clicks) {
                    // odd  
                } else {
                    // even clicks
                    e.preventDefault();
                    $('.donate-icon-wrap').addClass('hover');
                }
                $(this).data("clicks", !clicks);
            });
        }


        /*----------  Navigation  ----------*/

        $('#nav-icon').click(function() {
            $('.header').toggleClass('open');
            $('html').toggleClass('no-scroll');
        });

        // mobile nav
        if ($(window).width() < 992) {
            $('.menu-item-has-children > a').click(function(a) {

                a.preventDefault();
                a.stopPropagation();

                if ($(this).parent().hasClass("active")) {
                    $(this).parent().removeClass("active");
                    $(this).siblings(".sub-menu").slideUp(300, "linear");
                } else {
                    $(".menu-item-has-children").removeClass("active");
                    $(this).parent().addClass("active");
                    $(".sub-menu").slideUp(300, "linear");
                    $(this).siblings(".sub-menu").slideDown(300, "linear");

                }
            });
        }

        // Hide Header on on scroll down
        'use strict';

        var c, currentScrollTop = 0,
            navbar = $('.header:not(.open)');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 30) {
                $('.header').addClass('background-dark-important');
            } else {
                $('.header').removeClass('background-dark-important');
            }
            var a = $(window).scrollTop();
            var b = navbar.height();

            currentScrollTop = a;
            if (c < currentScrollTop && a > b + b) {
                navbar.addClass("scrollUp");
            } else if (c > currentScrollTop && !(a <= b)) {
                navbar.removeClass("scrollUp");
            }
            c = currentScrollTop;
        });


        /*----------  Accordion  ----------*/


        $('.accordion-list').on('click', '.toggle-question', function() {

            if ($(this).closest('.accordion-list').not('accordion-opened')) {
                $(this).toggleClass('accordion-opened');
                $(this).closest('.accordion-list').find('.accordion-content').slideToggle();
            } else {
                $('.accordion-list').removeClass('accordion-opened');
                $('.accordion-content').slideUp();
            }
        })


        /*----------  Slick slider  ----------*/
    
        var singleSlider = $(".single-item-slider");

        singleSlider.each(function() {
        
        var $slickElement = $('.single-item-slider');
    
        $slickElement.on('load init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            console.log($(this).parent().find('.slick-controls--single-item-slider .pagingInfo') );
            if (slick.slideCount > 1) {
            $(this).parent().find('.slick-controls--single-item-slider .pagingInfo').html('<strong>' + '0' + i + '</strong> / 0' + slick.slideCount);
            } 
        });
    
        $(this).slick({
            cssEase: 'linear',
            arrows: true,
            adaptiveHeight: true,
            prevArrow: $(this).parent().find(".slick-controls--single-item-slider .prev"),
            nextArrow: $(this).parent().find(".slick-controls--single-item-slider .next"),
        });
        });

        // $('.editor figure > a').attr('data-rel', 'lightcase:myCollection');
        // $('.editor  a img').parent().attr('data-rel', 'lightcase:myCollection');
        // $('.editor .grid-gallery a').attr('data-rel', 'lightcase:myCollection');
        // $('.editor figure a').attr('data-rel', 'lightcase:myCollection');
        //  $('.editor .wp-block-gallery a').attr('data-rel', 'lightcase:myCollection');

        $('a[data-rel^=lightcase]').lightcase({
           
            slideshowAutoStart: false,
            showSequenceInfo: false,
            transition: 'fade',
            speedIn: 50,
            disableShrink: true,
            speedOut: 50,
            maxHeight: 1600,
            maxWidth: 1600,
          });
    });

})(jQuery);