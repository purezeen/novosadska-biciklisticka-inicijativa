(function($) {

    $(document).ready(function() {

        mapboxgl.accessToken = 'pk.eyJ1IjoiaWthOTAiLCJhIjoiY2tmNTltYTZ4MGtyMTJzbWR3ZW52NzV0ZCJ9.JL3zupuuBokfmkpQMwxb6A';
        if ($('#map').length) {
            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/light-v10',
                center: [19.831398, 45.273755],
                zoom: 15,
            });

            map.on('load', function() {
                map.loadImage(
                    '' + stylesheet_directory_uri + '/img/pin.png',
                    function(error, image) {
                        if (error) throw error;
                        map.addImage('cat', image);
                        map.addSource('point', {
                            'type': 'geojson',
                            'data': {
                                'type': 'FeatureCollection',
                                'features': [{
                                    'type': 'Feature',
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': [19.831398, 45.273755]
                                    }
                                }]
                            }
                        });
                        map.addLayer({
                            'id': 'points',
                            'type': 'symbol',
                            'source': 'point',
                            'layout': {
                                'icon-image': 'cat',
                                'icon-size': 0.6
                            }
                        });
                    }
                );
            });
        }
        /*----------  Map  ----------*/

        if ($('div').hasClass('map-legend-wrap')) {

            var map_legend = new mapboxgl.Map({
                container: 'map-legend',
                style: 'mapbox://styles/mapbox/light-v10',
                center: [19.845727, 45.256819],
                zoom: 12
            });

            map_legend.on('load', function() {
                map_legend.addSource('route', {
                    "type": "geojson",

                    "data": {
                        "type": "Feature",
                        "properties": {},
                        'geometry': {
                            'type': 'LineString',
                            "coordinates": [
                                [19.825573371755866, 45.24008373899798],
                                [19.841395565137844, 45.243809830217174],
                                [19.837065843300564, 45.25201389683227],
                                [19.83241540132613, 45.249643118584316],
                                [19.830758347290413, 45.24922916302813],
                                [19.829475466745095, 45.249153898057386],
                                [19.8246646647043, 45.24930442789912],
                                [19.824076677787616, 45.25784634270184],
                                [19.822954157310875, 45.25893750798738],
                                [19.81156859247932, 45.25573920556735],
                                [19.822740343886892, 45.23955679728516],
                                [19.82546646504386, 45.239933184721565],
                                [19.8246646647043, 45.24934206029718]
                            ]
                        }
                    }
                });
                map_legend.addLayer({
                    'id': 'route',
                    'type': 'line',
                    'source': 'route',
                    'layout': {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    'paint': {
                        'line-color': 'red',
                        'line-width': 3
                    }
                });
            });

            // add markers to map
            geojson.features.forEach(function(marker) {

                // create a HTML element for each feature
                var el = document.createElement('div');
                el.className = 'marker';
                el.style.backgroundImage = 'url(' + marker.properties.icon + ')';
                // make a marker for each feature and add to the map
                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .addTo(map_legend);

                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .setPopup(new mapboxgl.Popup({
                            offset: 25
                        }) // add popups
                        .setHTML('<h6>' + marker.properties.title + '</h6><p>' + marker.properties.description + '</p>'))
                    .addTo(map_legend);
            });
        };
    });
})(jQuery);