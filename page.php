<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

   <?php
      if (has_post_thumbnail()) {
      
      ?>
      <div class="container">
         <div class="row">
            <div class="post-hero col-12 mb-4  pl-0 pr-0 <?php echo ( get_field( 'remove_green_border_on_hero_image' ) == 1 )? "" : "border-shadow"; ?>">
               <?php the_post_thumbnail('1536x1536', array('class' => 'nolazyloaded')); ?>
            </div>
         </div>
      </div>
      <?php
      }
   ?>
      
   <div class="container section pt-5 mt-5">
      <div class="row justify-content-center">
         <div class="col-10-12 col-md-9">

            <div class="line-left">
            <h1 class="color-dark-green mb-normal"><?php the_title(); ?></h1>

            <?php the_content(); ?>
            
            <br>
            </div>
         </div>
      </div>
	</div>
	
	<!-- Get acf flexible section  -->
   <?php get_template_part( 'template-parts/acf/flexible', 'section' ) ?>
   <!-- Endflexible section  -->


<div class="container section">
   <div class="single-item-slider-wrap">
      <div class="single-item-slider">
            <div>
               <a href="">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/about-hero.png"></img>
                  <div class="single-item-slider__caption center w-100 center">
                     <p>Some text </p>
                  </div>
               </a>
         </div>
         <div>
               <a href="">
               <img src="<?php echo get_template_directory_uri(); ?>/img/blog3.png"></img>
               </a>
         </div>
         <div>
               <a href="">
               <img src="<?php echo get_template_directory_uri(); ?>/img/post-hero.png"></img>
               </a>
         </div>
      </div>
   
      <div class="paginator text-color text-center slick-controls slick-controls--single-item-slider mt-4">
         <ul class="unstyle-list">
            <li class="prev"></li>
            <span class="pagingInfo"></span>
            <li class="next"></li>
         </ul>
      </div>
   </div>
</div>

<div class="container section">
   <div class="single-item-slider-wrap">
      <div class="single-item-slider">
            <div>
               <a href="">
               <img src="<?php echo get_template_directory_uri(); ?>/img/about-hero.png"></img>
               </a>
         </div>
         <div>
               <a href="">
               <img src="<?php echo get_template_directory_uri(); ?>/img/about-hero.png"></img>
               </a>
         </div>
         <div>
               <a href="">
               <img src="<?php echo get_template_directory_uri(); ?>/img/about-hero.png"></img>
               </a>
         </div>
      </div>
   
      <div class="paginator text-color text-center slick-controls slick-controls--single-item-slider mt-4">
         <ul class="unstyle-list">
            <li class="prev"></li>
            <span class="pagingInfo"></span>
            <li class="next"></li>
         </ul>
      </div>
   </div>
</div>

<?php
get_footer();
