<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gulp-wordpress
 */

get_header(); ?>


		<main id="main" class="site-main" role="main">
		    <div class="search__content">
		    <div class="container">
				<?php
				if ( have_posts() ) : ?>

					<header class="page-header">
						<h3 class="page-title color-dark-green"><?php printf( esc_html__( 'Rezultat pretrage za: %s', 'nsbi' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile;
					?>

                    <div class="pagination">
                       <?php pagination_nav(); ?>
                    </div> 

           <?php
				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
            </div>
			</div>


<?php
get_footer();
