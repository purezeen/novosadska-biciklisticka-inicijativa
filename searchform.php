<!-- <form role="search" method="get" id="searchform" class="searchform" action="<?php //echo esc_url( home_url( '/' ) ); ?>">
   <div>
      <label class="screen-reader-text searchform-label" for="s"><?php //_x( 'Search for:', 'label' ); ?>
      <input type="text" value="<?php //echo get_search_query(); ?>" name="s" id="s" />
      <input type="submit" id="searchsubmit" value="" />
      </label>
   </div>
</form> -->

<div class="search-wrap">
	
   <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
     
      <label class="search-input" for="inpt_search" for="s"><?php _x( 'Search for:', 'label' ); ?>
         <input id="inpt_search" type="text" placeholder="Pretraga" value="<?php echo get_search_query(); ?>" name="s" id="s" />
      </label>
      
   </form>
	
</div>