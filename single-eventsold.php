<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */
get_header(); ?>

<main class="container event-single">
   <div class="row">
      <div class="event-single_header col-12">
         <div class="event_data">
            <div class="big">
               <p> 29</p> <span><br>FEB</span>
            </div>
            <div class="small">
               <em>-</em> 26<span><br>JUL</span>
            </div>
         </div>

         <div class="event-single_header_inner">
            <h1>masterclass svesne pažnje</h1>
            <p>Bolja koncentracija, memorija i fokus, svesniji odnosi sa drugima i nama samima.</p>
         </div>

         <a href="#form" class="btn-full btn-light-green">Prijavi se</a>
      </div>

      <div class="post-hero col-12">
         <img src="<?php echo get_template_directory_uri()?>/img/event-promo.png" alt="">
      </div>

      <div class="event-single_top_info col-12">
         <div>
            <h5>Vreme</h5>
            <p class="icon-time-grey relative">20 (Friday) 6:00 pm - 22 (Sunday) 10:00 pm</p>
         </div>
         <div>
            <h5>Mesto</h5>
            <p class="icon-location-grey">Irving City Park, NE Fremont St, Portland, OR 97212</p>
         </div>
      </div>

      <div class="col-12 editor">
         <?php		
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
         ?>

         <div class="clearfix" id="share">
            <?php echo do_shortcode('[addtoany]');  ?>
         </div>

      </div>
   </div>

   <section class="post-author event-author">
      <div class="row align-items-center">
         <div class="col-12 col-md-5 col-lg-3">
            <div class="post-author_image">
               <img src="<?php echo get_template_directory_uri()?>/img/predavac.png" alt="">
            </div>
         </div>

         <div class="col-12 col-md-7 col-lg-8 post-author_content">
            <span>PREDAVAČ</span>
            <h3>Vesna Perić</h3>
            <em>CEO of Pure Engine</em>
            <p>Moje ime je Vesna, po struci sam pedagog i mindfulness trener.
               Moj put otkrivanja i bavljenja Mindfulness-om je bio pre svega zabavan, dinamičan i sa mnogo uspona i
               padova. Podeliću sa vama deo moje lične priče,a zatim zablude i pogrešne informacije koje osobe često
               imaju
               vezano za praksu svesne pažnje, kao i neke od vežbi koje su se pokazale više nego korisnim brojnim
               polaznicima Mindfulness treninga.
            </p>
         </div>
      </div>
   </section>

   <section class="section background-grey" id="form">
      <div class="section-form center form-two-columns clearfix">
         <div class="section-form_header">
            <h5>Prijavi se za događaj</h5>
            <p>Pridruži se besplatnom Master klasu sa Vesnom koji se održava u narednih 7 dana!</p>
         </div>
         <?php echo do_shortcode('[contact-form-7 id="56" title="Prijavi se za dogadjaj"]') ?>
      </div>
   </section>
</main>

<section class="section">
   <div class="container">

      <div class="event-list relative">

         <a class="event">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p> <span><br>JUL</span>
                  </div>
                  <div class="small">
                     <em>-</em> 26<span><br>JUL</span>
                  </div>
               </div>
               <div class="event_type">predavanje</div>
            </div>
            <div class="event_content">
               <h4>SLIDEDOWN EVENT</h4>
               <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
               <p>Irving City Park, NE Fremont St, Portland, OR 97212</p>
            </div>
         </a>

         <a class="event event_mark">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p><span><br>JUL</span>
                  </div>
                  <div class="small"><em>-</em> 26<span><br>JUL</span></div>
               </div>
            </div>
            <div class="event_content">
               <h4>EVENT WITH FEATURED IMAGE</h4>
               <p>4:00 Pm - 10:00 Pm</p>
               <p>Portland Bridges, 400 Southwest Kingston Avenue Portland, OR</p>
            </div>
         </a>

         <a class="event">
            <div class="event-header">
               <div class="event_data">
                  <div class="big">
                     <p> 16</p> <span><br>JUL</span>
                  </div>
                  <div class="small">
                     <em>-</em> 26<span><br>JUL</span>
                  </div>
               </div>
               <div class="event_type">predavanje</div>
            </div>
            <div class="event_content">
               <h4>SLIDEDOWN EVENT</h4>
               <p>(July 16) 6:00 Am - (September 26) 9:00 Am</p>
               <p>Irving City Park, NE Fremont St, Portland, OR 97212</p>
            </div>
         </a>

      </div>

      <div class="row justify-content-center">
         <div class="col-12 text-center">
            <a href="" class="btn-full btn-light-green btn-center">Ostali događaji</a>
         </div>
      </div>


   </div>
</section>
<?php
get_footer();