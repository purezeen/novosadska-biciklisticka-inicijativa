<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */
get_header(); ?>

<!-- Post single  -->
<div id="primary" class="content-area container single-team">
   <main id="main" class="site-main" role="main">
      <div class="row">
      <div class="post-hero border-shadow col-12 pl-0 pr-0">
      <?php if( has_post_thumbnail() ): ?>
         <div class="aspect-radio-16-9"><?php the_post_thumbnail('1536x1536', array('class' => 'nolazyloaded')); ?></div>
      <?php endif; ?>
      </div>
      </div>
     
      <div class="row justify-content-center mt-md-5 single-team__content" >
         <div class="col-10-12 col-md-9">
         
            <?php		
            while ( have_posts() ) : the_post();
            ?>
               <div class="post-header">
                  <h1 class="color-dark-green"><?php the_title(); ?> </h1>
                  <p class="subtitle"><?php the_field( 'job_title' ); ?></p>
                  <hr>
               </div>
               <?php the_content(); ?>

               <div class="clearfix mt-5 pt-5" id="share">
                  <?php echo do_shortcode('[addtoany]');  ?>
               </div>

            <?php
            endwhile; // End of the loop.
            ?>

         </div>
      </div>

   </main><!-- #main -->
</div><!-- #primary -->
<!-- End post single  -->

<?php
get_footer();


