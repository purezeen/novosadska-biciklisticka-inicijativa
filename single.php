<?php
/**
 * The template for displaying single post.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); 

$category_post = get_the_category(); 
$tags_post = get_the_tags();
?>

<!-- Post single  -->
<main id="main" class="site-main" role="main">

<!-- Single post content -->
<div class="single-post__content container">

   <div class="single-post__content-main">

      <!-- Single post hero -->
      <div class="single-post__hero">
         <?php  if (has_post_thumbnail()) {
            ?>
            <div class="post-hero">
               <div class="featured-post__info">
               <div class="featured-post__author"><?php _e( 'Autor', 'nsbi' ); ?>:<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><strong><?php echo get_the_author_meta('user_firstname'); ?> <?php echo get_the_author_meta('user_lastname'); ?></strong></a></div>            
               <div class="featured-post__date"><?php _e( 'Članak objavljen', 'nsbi' ); ?> <strong class="date"><?php $post_date = get_the_date( "j. F Y.", get_the_ID() ); echo $post_date; ?></strong><strong class="date-mobile"><?php $post_date = get_the_date( "d.m.Y.", get_the_ID() ); echo $post_date; ?></strong></div>
               </div>

               <div class="post-hero__img">
               <?php the_post_thumbnail('1536x1536', array('class' => 'nolazyloaded')); ?>
               </div>
               <div class="featured-post__banner">
         
               <?php
               $category_post = get_the_category(); 
               foreach ($category_post as $cat) {
                  if ($cat->slug != 'objave') {
                     ?>
                     <a href="<?php echo get_category_link($cat->term_id)?>" class="cat-box">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cat-icon-white.png"><span class="cat-box__item" alt="icon" width="16" height="16"><?php echo $cat ->name; ?></span>
                     </a>
                  <?php
                  }
                  }
               ?>
                           
               <?php
               $tags_post = get_the_tags();
               if ( $tags_post ) : ?>
                  <div class="tag-box"> <img src="<?php echo get_template_directory_uri(); ?>/img/tag-icon-white.png" alt="icon" width="20" height="20">
                  <?php foreach ( $tags_post as $tag ) : ?>
                           <a class="tag-box__item" href="<?php echo get_tag_link($tag->term_id)?>"><?php echo  $tag->name; ?></a>
                  <?php endforeach; ?>
                  </div>
               <?php endif; ?>

               </div>
            </div>
            <?php
         }?>
      </div>
      <!-- End single post hero -->


      <div class="post-header">
         <h1 class="color-dark-green"><?php the_title(); ?> </h1>
         <p class="subtitle"><?php the_field( 'subtitle' ); ?></p>
         <hr>
      </div>

      <div class="editor">
         <?php the_content(); ?>
      </div>
   

      <!-- Post author  -->
      <section class="post-author">

         <div class="clearfix" id="share">
            <?php echo do_shortcode('[addtoany]');  ?>
         </div>

         <div class="post-author-box">

         <div class="row">
         <div class="col-12 col-md-3 col-lg-2">
            <div class="post-author_image">
               <img src="<?php echo get_avatar_url( $author, 135 ); ?>" alt="Author Img">
            </div>
         </div>
         <div class="col-12 col-md-7 col-lg-10">
            <div class="post-author_content">
               <?php $author = $post->post_author; ?>
               <span><?php _e( 'O AUTORU', 'nsbi' ); ?></span>
               <h4>
                  <?php echo get_the_author_meta('first_name', $author); ?>&nbsp;<?php echo get_the_author_meta('last_name', $author); ?>
               </h4>
               <p><?php echo get_the_author_meta('description', $author); ?></p>
            </div>
         </div>
         </div>

         </div>

      </section>
      <!-- End post author  -->

   </div>
   <!-- End single-post__content-main -->

   <div class="single-post__content-bottom">
      <!-- Latest posts  -->
      <div class="three-cards__wrap section">
   
         <h2 class="mb-big text-center color-dark-green"><?php _e( 'Slične vesti', 'nsbi' ); ?></h2>
   
         <!-- Get Latest news  -->
         <?php get_template_part('template-parts/latest', 'news'); ?>
         <!-- End Latest news  -->
   
      </div>
      <!-- End latest posts  -->
   </div>
   <!-- end single-post__content-bottom -->

   <?php get_sidebar(); ?>

</div>
<!-- End Single post content  -->


<?php
get_footer();