<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<!-- Post single  -->
<div id="primary" class="content-area container">
   <main id="main" class="site-main row" role="main">

      <?php  if (has_post_thumbnail()) {
         ?>
         <div class="post-hero border-shadow col-12">
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
         </div>
      <?php
      }?>

      <div class="post-header col-12">
         <h1 class="color-dark-green"><?php the_title(); ?> </h1>
         <p class="subtitle"><?php the_field( 'subtitle' ); ?></p>
         <hr>
      </div>

      <div class="col-12 editor">
         <?php		
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>

      </div>
   </main>
</div>
<!-- End post single  -->

<!-- Post author  -->
<section class="post-author container">
   <div class="row">
      <div class="col-12">
         <div class="clearfix" id="share">
            <?php echo do_shortcode('[addtoany]');  ?>
         </div>
      </div>
   </div>

   <div class="row post-author-box">
      <div class="col-12 col-md-3 col-lg-2">
         <div class="post-author_image">
            <img src="<?php echo get_avatar_url( $author, 135 ); ?>" alt="Author Img">
         </div>
      </div>
      <div class="col-12 col-md-7 col-lg-10 post-author_content">
         <?php $author = $post->post_author; ?>
         <span>O AUTORU</span>
         <h4>
            <?php echo get_the_author_meta('first_name', $author); ?>&nbsp;<?php echo get_the_author_meta('last_name', $author); ?>
         </h4>
         <p><?php echo get_the_author_meta('description', $author); ?></p>
      </div>
   </div>
</section>
<!-- End post author  -->
<?php 
   $contact_form_heading   = get_field( 'contact_form_heading' );
   $contact_form_subtitle  = get_field( 'contact_form_subtitle' );
   $contact_form_shortcode = get_field( 'contact_form_shortcode' );
?>
<?php if ( $contact_form_heading || $contact_form_subtitle || $contact_form_shortcode ): ?>

<section class="section background-grey container" id="form">
   <div class="section-form center form-two-columns clearfix">
      <div class="section-form_header">
         <h3 class="color-dark-green"><?php echo $contact_form_heading; ?></h3>
         <p><?php echo $contact_form_subtitle; ?></p>
      </div>
      <?php echo $contact_form_shortcode; ?>
   </div>
</section>
<?php endif ?>
<!-- Latest posts  -->

<!-- <div class="container">
<?php //comments_template(); ?> 
</div> -->


<section class="latest-post container section">
   <div class="row">
      <div class="col-12 d-flex justify-content-center">
         <h2 class="section-title mb-big color-dark-green">Slične vesti</h2>
      </div>
   </div>

   <!-- Get Latest news  -->
   <?php get_template_part('template-parts/latest', 'news'); ?>
   <!-- End Latest news  -->
   
</section>
<!-- End latest posts  -->

<?php
get_footer();