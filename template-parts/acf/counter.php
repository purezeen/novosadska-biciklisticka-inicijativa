<!-- Counter section  -->
<?php if ( have_rows( 'counter_section', 'option' ) ) : ?>
<section class="section-small background-dark counter" id="counter">
   <div class="container">
      <div class="counter row">
         <?php while ( have_rows( 'counter_section', 'option' ) ) : the_row(); ?>

         <div class="counter__box col-6 col-sm-3">

            <?php $icon = get_sub_field( 'icon' ); ?>
            <?php if ( $icon ) { ?>
            <div class="counter__icon">
               <img src="<?php echo $icon['sizes']['thumbnail']; ?>" alt="">
            </div>
            <?php } ?>
            <div class="counter__number-wrap">
               <h3><span class="counter__number" id="counter__number"
                     data-count="<?php the_sub_field( 'number' ); ?>">0</span>+</h3>
            </div>
            <p><?php the_sub_field( 'title' ); ?></p>
         </div>
         <?php endwhile; ?>
         <?php else : ?>
         <?php // no rows found ?>
         <?php endif; ?>
      </div>
   </div>
</section>
<!-- End counter section  -->

