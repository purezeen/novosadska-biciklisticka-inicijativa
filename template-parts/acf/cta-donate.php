<!-- Section cta  -->
<?php if ( have_rows( 'cta_section', 'option' ) ) : ?>
   <section class="cta-box background-green">
      <div class="container">
        
         <?php while ( have_rows( 'cta_section', 'option' ) ) : the_row(); ?>
            <div class="row">
               <div class="col-12 col-md-7">
                  <h3><?php the_sub_field( 'cta_title' ); ?></h3>
                  <p><?php the_sub_field( 'cta_text' ); ?></p>
                  <?php $cta_button = get_sub_field( 'cta_button' ); ?>
                  <?php if ( $cta_button ) { ?>
                     <a href="<?php echo $cta_button['url']; ?>" target="<?php echo $cta_button['target']; ?>" class="btn btn-full btn-dark-green btn-icon mt-5"><?php echo $cta_button['title']; ?></a>
                  <?php } ?>
               </div>
            </div>
         <?php endwhile; ?>
         <div class="bicycle-icon">
            <img src="<?php echo get_template_directory_uri()?>/img/bicycle-icon.svg" alt="doniraj"  width="330" height="200">
         </div>
      </div>
   </section>

<?php endif; ?>
<!-- End section cta  -->


       
     


