<?php if ( have_rows( 'flexible_section' ) ): ?>
<?php while ( have_rows( 'flexible_section' ) ) : the_row(); ?>

<!-- Two columns - border box  -->
<?php if ( get_row_layout() == 'two_columns_-_border_box' ) : ?>
<section class="container section">
   <div class="row">

      <div class="col-12">
         <h3 class="section-title-smaller text-center color-dark-green"><?php the_sub_field( 'section_title' ); ?></h3>
      </div>

      <?php if ( have_rows( 'two_columns_-_border_box' ) ) : ?>
      <?php while ( have_rows( 'two_columns_-_border_box' ) ) : the_row(); ?>

      <div class="col-12 col-md-6 d-flex">
         <div class="border-box">
            <h5 class="border-box-title"><?php the_sub_field( 'box_title' ); ?></h5>
            <?php if ( have_rows( 'text' ) ) : ?>
            <?php while ( have_rows( 'text' ) ) : the_row(); ?>
            <?php the_sub_field( 'text' ); ?>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <?php endwhile; ?>
      <?php else : ?>
      <?php // no rows found ?>
      <?php endif; ?>
   </div>
</section>
<!-- End two columns - border box  -->


<?php elseif ( get_row_layout() == 'two_columns_masonry' ) : ?>

<section class="background-grey section">
   <div class="container">
            <h3 class="section-title-smaller color-dark-green text-center"><?php the_sub_field( 'section_title' ); ?>
            </h3>
   </div>

   <?php if ( have_rows( 'two_columns_masonry' ) ) : ?>

   <div class="container">
   <div class="row">
  
      <?php while ( have_rows( 'two_columns_masonry' ) ) : the_row(); ?>
      <div class="col-md-6 d-flex">
      <div class="white-box">


         <?php $icon = get_sub_field( 'icon' ); ?>

         <?php if ( $icon ) { ?>
         <div class="white-box__icon">
            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" width="48" height="48"/>
         </div>
         <?php } ?>

         <h6><?php the_sub_field( 'title' ); ?></h6>
         <?php the_sub_field( 'text' ); ?>

         <?php if ( have_rows( 'cta' ) ) : ?>
         <div class="btn-group btn-group-dark-green">
            <?php while ( have_rows( 'cta' ) ) : the_row(); ?>
            <?php $cta = get_sub_field( 'cta' ); ?>
            <?php if ( $cta ) { ?>
            <a class="btn" href="<?php echo $cta['url']; ?>"
               target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>
            <?php } ?>
            <?php endwhile; ?>
         </div>
            
         <?php else : ?>
         <?php // no rows found ?>
         <?php endif; ?>

      </div>
      </div>
      <?php endwhile; ?>
      <?php else : ?>
      <?php // no rows found ?>
      <?php endif; ?>
   
   </div>
   </div>
</section>

<!-- Three columns - border box  -->
<?php elseif ( get_row_layout() == 'three_columns_-__border_box' ) : ?>
<section class="container section">
  
         <h3 class="section-title-smaller color-dark-green text-center"><?php the_sub_field( 'section_title' ); ?></h3>

   <?php if ( have_rows( 'columns' ) ) : ?>
   <div class="row icon-box-container">

      <?php while ( have_rows( 'columns' ) ) : the_row(); ?>
      <div div class="col-12 col-md-6 col-lg-4 d-flex">
         <div class="icon-box">
            <?php $icon = get_sub_field( 'icon' ); ?>
            <?php if ( $icon ) { ?>
            <div class="icon-box-img">
               <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
            </div>
            <?php } ?>
            <h6 class="icon-box-title"><?php the_sub_field( 'box_title' ); ?></h6>
            <?php if ( have_rows( 'text' ) ) : ?>
            <?php while ( have_rows( 'text' ) ) : the_row(); ?>
            <?php the_sub_field( 'text' ); ?>
            <?php endwhile; ?>
            <?php endif; ?>

         </div>
      </div>
      <?php endwhile; ?>

   </div>
   <?php else : ?>
   <?php // no rows found ?>
   <?php endif; ?>
</section>
<!-- End three columns - border box  -->

<!-- Faq  -->
<?php elseif ( get_row_layout() == 'faq' ) : ?>
<section class="section accordion">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 col-md-8">
            <h3 class="color-dark-green mb-5"><?php the_sub_field( 'section_title' ); ?></h3>
         </div>

         <?php if ( have_rows( 'faq' ) ) : ?>
         <ul class="list-border accordion-closed col-12 col-md-8">
            <?php while ( have_rows( 'faq' ) ) : the_row(); ?>

            <li class="accordion-list">

               <span class="toggle-question">
                  <div class="circle-plus">
                     <div class="horizontal"></div>
                     <div class="vertical"></div>
                  </div>
                  <?php the_sub_field( 'question' ); ?>
               </span>
               <div class="accordion-content"><?php the_sub_field( 'answer' ); ?></div>
            </li>
            <?php endwhile; ?>
            <?php else : ?>
            <?php // no rows found ?>
         </ul>
         <?php endif; ?>
      </div>
   </div>
</section>
<!-- End faq  -->

<!-- Counter section  -->
<?php elseif ( get_row_layout() == 'counter_section' ) : ?>
<?php get_template_part( 'template-parts/acf/counter' ) ?>
<!-- End counter section  -->

<!-- Two columns  -->
<?php elseif ( get_row_layout() == 'two_columns_with_image_an' ) : ?>
<?php if ( have_rows( 'two_columns' ) ) : ?>
<section class="container section">

   <?php while ( have_rows( 'two_columns' ) ) : the_row(); ?>

   <?php if ( get_sub_field( 'column_reverse' ) == 1 ) { 
                        $column_reverse = "two-columns-reverse";
                     } else { 
                        $column_reverse = "";
                     } ?>

   <div class="row two-columns align-items-center <?php echo $column_reverse; ?> ">
      <?php $image = get_sub_field( 'image' ); ?>
      <?php if ( $image ) { ?>

      <div class="col-12 col-md-6 mb-5 mb-md-0">
         <div class="section-two-column-img">
            <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
         </div>
      </div>

      <?php } ?>
      <?php if ( have_rows( 'content' ) ) : ?>
      <?php while ( have_rows( 'content' ) ) : the_row(); ?>

      <div class="col-12 col-md-6 section-two-column-content">
         <h3 class="color-dark-green"><?php the_sub_field( 'title' ); ?></h3>
         <p><?php the_sub_field( 'text' ); ?></p>
         <?php $cta = get_sub_field( 'cta' ); ?>
         <?php if ( $cta ) { ?>
         <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"
            class="btn btn-full btn-light-green btn-center"><?php echo $cta['title']; ?></a>
         <?php } ?>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
   </div>
   <?php endwhile; ?>
</section>
<?php else : ?>
<?php // no rows found ?>
<?php endif; ?>
<!-- End two columns  -->

<!-- Slider section  -->
<?php elseif ( get_row_layout() == 'slider' ) : ?>
<section class="services-slider section background-grey">

   <?php 
   if(get_sub_field( 'section_title' )) {
      ?>
      <div class="row">
         <div class="col-12 d-flex justify-content-center">
            <h2 class="color-black mb-big"><?php the_sub_field( 'section_title' ); ?></h2>
         </div>
      </div>
      <?php
   }
   ?>
   
   <?php if ( have_rows( 'slider' ) ) : ?>
   <div class="container">
      <div class="row">
         <div class="single-item-slider col-12">

               <?php while ( have_rows( 'slider' ) ) : the_row(); ?>
                  <div>
                     <div class="container background-white">
                        <div class="row justify-content-center">
                           <div class="col-12 col-sm-7">
                              <div class="services_content line-left">
                                 <h3><?php the_sub_field( 'slider_title' ); ?></h3>
                                 <?php the_sub_field( 'slider_text' ); ?>

                                 <?php if ( have_rows( 'slider_cta' ) ) : ?>
                                    <div class="btn-group btn-group-light-green">
                                       <?php while ( have_rows( 'slider_cta' ) ) : the_row(); ?>
                                          <?php $cta = get_sub_field( 'cta' ); ?>
                                          <?php if ( $cta ) { ?>
                                          <a href="<?php echo $cta['url']; ?>" class="btn"
                                             target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>
                                          <?php } ?>
                                       <?php endwhile; ?>
                                    </div>
                                 <?php endif; ?>
                              
                              </div>
                           </div>

                           <?php $slider_image = get_sub_field( 'slider_image' ); ?>
                           <?php if ( $slider_image ) { ?>
                              <div class="col-12 col-sm-5 services-image cover"
                                 style="background-image: linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%), url(<?php echo $slider_image['url']; ?>)">
                              </div>
                           <?php } ?>
                        </div>
                     </div>
                  </div>

               <?php endwhile; ?>
               
           
         </div>
         <div class="paginator text-color text-center slick-controls slick-controls--single-item-slider mt-4">
            <ul class="unstyle-list">
               <li class="prev"></li>
               <span class="pagingInfo"></span>
               <li class="next"></li>
            </ul>
         </div>
      </div>
   </div>
   <?php endif; ?>

</section>    

<!-- End slider section  -->

<!-- Masonry grid  -->
<?php elseif ( get_row_layout() == 'masonry_grid' ) : ?>
<?php if ( have_rows( 'masonry_card' ) ) : ?>

   <section class="container section">
      <div class="news-masonry grid">
         <div class="grid-sizer"></div>
   
         <?php while ( have_rows( 'masonry_card' ) ) : the_row(); ?>
               <?php $masonry_card_title = get_sub_field( 'masonry_card_title' ); ?>
               <?php $masonry_card_content = get_sub_field( 'masonry_card_content' ); ?>
               <?php $Masonry_card_link = get_sub_field( 'Masonry_card_link' ); ?>
             
            <div class="item grid-item">
               <?php if ( $Masonry_card_link ) { ?>
                  <a href="<?php echo $Masonry_card_link['url']; ?>" class="news-masonry__card d-block <?php echo (get_sub_field( 'masonry_card_background' ) == 1) ? 'background-green color-white' : ''; ?>">
               <?php } else { ?>
                  <div class="news-masonry__card d-block <?php echo (get_sub_field( 'masonry_card_background' ) == 1) ? 'background-green color-white' : ''; ?>">
               <?php } ?>

                  <?php $masonry_card_image = get_sub_field( 'masonry_card_image' ); ?>
                  <?php if ( $masonry_card_image ) { ?>
                     <div class="news-masonry__card-img">
                        <img src="<?php echo $masonry_card_image['url']; ?>" alt="<?php echo $masonry_card_image['alt']; ?>" />
                     </div>
                  <?php } ?>
                  <?php if(!empty($masonry_card_title) || !empty($masonry_card_content)): ?>
                     <div class="news-masonry__card-content">
                        <?php if(!empty($masonry_card_title)): ?>
                           <h6 class="color-dark-green news-masonry__card-title"><?php echo $masonry_card_title; ?></h6>
                        <?php endif; ?>
                        <?php if(!empty($masonry_card_content)): ?>
                           <p><?php echo $masonry_card_content; ?></p>
                           <?php if ( $Masonry_card_link ) { ?>
                              <span class="btn-link" target="<?php echo $Masonry_card_link['target']; ?>"><?php echo $Masonry_card_link['title']; ?></span>
                           <?php } ?>
                        <?php endif; ?>
                        
                     </div>
                  <?php endif; ?>
                  <?php if ( $Masonry_card_link ) { ?>
                  </a>
                     <?php } else { ?>
                     </div>
                  <?php } ?>
            </div>
         <?php endwhile; ?>

      </div>
   </section>
<!-- End Masonry grid  -->

<?php endif; ?>

<?php endif; ?>
<?php endwhile; ?>
<?php else: ?>
<?php // no layouts found ?>
<?php endif; ?>
