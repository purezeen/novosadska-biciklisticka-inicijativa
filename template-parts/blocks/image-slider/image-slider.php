<?php if ( have_rows( 'image_slider' ) ) : ?>
   <div class="section">
      <div class="single-item-slider-wrap">
         <div class="single-item-slider">
            <?php while ( have_rows( 'image_slider' ) ) : the_row(); ?>
               <div>
                  <?php $image_slider_row = get_sub_field( 'image_slider_row' ); ?>
                  <?php $image_slider_caption = get_sub_field( 'image_slider_caption' ); ?>
                  
                  <?php if ( $image_slider_row ) { ?>
                     <a href="<?php echo $image_slider_row['url']; ?>" data-rel="lightcase:myCollection">
                        <img src="<?php echo $image_slider_row['url']; ?>" alt="<?php echo $image_slider_row['alt']; ?>">
                        <?php if($image_slider_caption): ?>
                           <div class="single-item-slider__caption center w-100 center">
                              <p><?php echo $image_slider_caption; ?></p>
                           </div>
                        <?php endif; ?>
                     </a>
                  <?php } ?>
                 
               </div>
            <?php endwhile; ?>
         </div>
   
         <div class="paginator text-color text-center slick-controls slick-controls--single-item-slider mt-4">
            <ul class="unstyle-list">
               <li class="prev"></li>
                  <span class="pagingInfo"></span>
               <li class="next"></li>
            </ul>
         </div>
      </div>
   </div>
<?php endif; ?>

