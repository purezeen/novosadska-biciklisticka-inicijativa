<?php

/**
 * Sidebar card
**/
?>

<?php $card_link = get_field( 'card_link' ); ?>
<?php if ( $card_link ) { ?>
<a href="<?php echo $card_link['url']; ?>" target="<?php echo $card_link['target']; ?>" class="sidebar-card">

<?php $card_image = get_field( 'card_image' ); ?>
<?php if ( $card_image ) { ?>
    <div class="sidebar-card__image">
    	<img src="<?php echo $card_image['url']; ?>" alt="<?php echo $card_image['alt']; ?>" />
    </div>
<?php } ?>

<?php $card_text = get_field( 'card_text' ); ?>
<?php if ( $card_text ) { ?>
<div class="sidebar-card__text">
     <?php echo $card_text; ?>
</div>
<?php } ?>

<?php $card_link_title=$card_link['title']; if ( $card_link_title ) { ?>
<div class="sidebar-card__btn">
     <span class="btn-link btn-link_green btn-arrow"><?php echo $card_link['title']; ?></span>
</div>
<?php } ?>

</a>
<?php } ?>