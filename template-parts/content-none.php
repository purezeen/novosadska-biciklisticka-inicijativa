<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Ništa nije pronađeno.', 'gulp-wordpress' ); ?></h1>
	</header><!-- .page-header -->
	<p><?php _e( 'Žao nam je, ali ništa se ne podudara sa vašim terminima za pretragu. Pokušajte ponovo sa drugim ključnim rečima.', 'nsbi' ); ?></p>
</section><!-- .no-results -->
