<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

   $cat_array = has_category('dogadaji');
   $category = get_the_category();

   // var_dump($cat);
   if($cat_array) {
      printf('prvo');
      ?>
      <!-- Start blog post block  -->
      <!-- Event blog post  -->
      <div class="row blog-row blog-event">
         <div class="col-12 col-sm-6 col-md-6">
            <?php
               if (has_post_thumbnail()) {
               $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
               $backgroundImg = $backgroundImg[0];
               }else {
                  $backgroundImg="";
               }
            ?>
            <div class="blog-page_image cover" style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo $backgroundImg;?>)">
               <div class="blog-event-content">
               <?php if(get_field( 'event_day_name' )) {
                  ?>
                  <h6><?php the_field( 'event_day_name' ); ?></h6>
                  <?php
               } ?>
                  
                  <h2 class="blog-event-data"><?php the_field( 'event_day' ); ?> <?php the_field( 'event_month' ); ?></h2>
                  <?php if ( have_rows( 'event_time' ) ) : ?>
                     <?php while ( have_rows( 'event_time' ) ) : the_row(); ?>

                     <?php if(get_sub_field( 'time_start' )) {
                        ?>
                        <p><span class="iconify" data-icon="carbon:time" data-inline="false"></span><span><?php the_sub_field( 'time_start' ); ?> <?php the_sub_field( 'time_end' ); ?></span></p> 
                        <?php
                     } ?>
                    
                     <?php endwhile; ?>
                  <?php endif; ?>
                  <?php if(get_field( 'event_location' )) {
                     ?>
                     <p><span class="iconify" data-icon="cil:location-pin" data-inline="false"></span><?php the_field( 'event_location' ); ?></p>
                     <?php
                  } ?>
                  
               </div>
            </div>
         </div>
      <?php
   }else {
      ?>
      <!-- Start blog post block  -->
      <div class="row">
         <div class="col-12 col-sm-6 col-md-6">
            <?php
               if (has_post_thumbnail()) {
                  $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
                  $backgroundImg = $backgroundImg[0];
               }else {
                  $backgroundImg="";
               }
            ?>

            <a href="#" class="post-item_image cover"
               style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url(<?php echo $backgroundImg;?>)">
            </a>

         </div>
      <?php
   }
 ?>
   <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center">
   <div class="post-item_content">
      <a href="<?php echo get_permalink();?>">
         <h4><?php the_title(); ?></h4>
      </a>
      <p class="subtitle"><?php $post_date = get_the_date( "m.d.Y.", get_the_ID() ); echo $post_date; ?> &nbsp; | &nbsp; 
      
      <?php
         foreach ($category as $cat) {
            if ($cat->slug != 'objave') {
               ?>
               <a href="<?php echo get_category_link($cat->term_id)?>"><?php echo $cat ->name; ?></a>
               <?php
            }
         }
      ?>
      </p>
      <p><?php the_excerpt(); ?></p>
      <a href="<?php echo get_permalink();?>" class="btn-link btn-link_green">Saznaj više</a>
   </div>
</div>
</div>

