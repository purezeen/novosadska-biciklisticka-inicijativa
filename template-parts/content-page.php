<?php
/**
 * Template part for displaying blog card in home-page and blog-page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

 $category = get_the_category();
 $tags = get_the_tags();

 ?>

<div class="post-item">
    
    <?php
        if (has_post_thumbnail()) {
           $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); 
           $backgroundImg = $backgroundImg[0];
        }else {
           $backgroundImg="";
        }
        ?>

    <div class="post-item_image aspect-ratio-bg cover"
        style="background-image:linear-gradient( rgba(14, 23, 16, .15) 100%, rgba(14, 23, 16, .15)100%) , url('<?php echo $backgroundImg;?>')">
        <?php foreach ($category as $cat) {
            if ($cat->slug != 'objave') { ?>
                <a href="<?php echo get_category_link($cat->term_id)?>" class="post-item__cat cat-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/cat-icon-white.png" alt="category" width="16" height="16"><span
                        class="cat-box__item"><?php echo $cat ->name; ?></span>
                </a>
            <?php } ?>
         <?php } ?>
    </div>

    <div class="post-item_content">
        <a href="<?php echo get_permalink();?>" >
             <h3 class="post-item__title"><?php the_title(); ?></h3>
        </a>
        <div class="post-item__info">
            <div class="post-item__date"><?php $post_date = get_the_date( "m.d.Y.", get_the_ID() ); echo $post_date; ?>
            </div>
            <div class="post-item__author"><?php _e( 'Autor', 'nsbi' ); ?>: <a class="author-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><strong><?php echo get_the_author_meta('user_firstname'); ?>
                    <?php echo get_the_author_meta('user_lastname'); ?></strong></a></div>

        </div>
        <?php the_excerpt(); ?>
        <div class="post-item__footer">

            <?php if ( $tags ) : ?>
            <div class="post-item__tags tag-box"> <img
                    src="<?php echo get_template_directory_uri(); ?>/img/tag-icon-green.png" alt="tag" width="20" height="20">
                <?php foreach ( $tags as $tag ) : ?>
                <a class="tag-box__item tag-box__item--green"
                    href="<?php echo get_tag_link($tag->term_id)?>"><?php echo $tag ->name; ?></a>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

            <a href="<?php echo get_permalink();?>" class="btn-link btn-link_green btn-arrow"><?php _e( 'Saznaj više', 'nsbi' ); ?></a>
        </div>
    </div>

    <a class="card-link" href="<?php echo get_permalink();?>"></a>
</div>