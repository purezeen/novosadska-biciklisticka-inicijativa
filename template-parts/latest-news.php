<!-- latest news -->
<?php
// WP_Query arguments
$args = array(
   'orderby'        => 'rand',
   'posts_per_page' => 3,
   'post__not_in' => array (get_the_ID())
);

// The Query
$the_query = new WP_Query( $args );

if ($the_query->have_posts()) : ?>
   <div class="three-cards">
      <div class="row">
      <?php while($the_query->have_posts()) : $the_query->the_post();  ?>
      <div class="col-md-6 col-lg-4 d-flex"> 

      <?php get_template_part( 'template-parts/content-page' ) ?>

      </div>
      <?php 
      endwhile;
      wp_reset_postdata(); 
      ?>
      </div>
   </div>

<?php endif; ?> 
<!-- end latest news -->