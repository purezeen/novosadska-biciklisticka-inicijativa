<?php /* Template Name: About us */ get_header(); ?>

<main id="main" class="site-main" role="main">
   
   <!-- Hero section  -->
   <?php
      if (has_post_thumbnail()) {
      ?>
      <div class="post-hero__wrap">
         <div class="container">
            <div class="post-hero">
               <div class="post-hero__img">
                  <?php the_post_thumbnail('1536x1536', array('class' => 'nolazyloaded')); ?>
               </div>
            </div>
         </div>
      </div>
      <?php
      }
   ?>
   <!-- End hero section  -->
   
   <!-- Page content  -->
   <section class="container section pb-0 pt-3 mt-3 pt-md-5 mt-md-5">
      <div class="row justify-content-center">
         <div class="col-10-12 col-md-9">

            <div class="line-left">
            <h1 class="color-dark-green mb-normal"><?php the_title(); ?></h1>

            <?php the_content(); ?>
           
            </div>
         </div>
      </div>
   </section>

   <!-- Get acf flexible section  -->
   <?php get_template_part( 'template-parts/acf/flexible', 'section' ) ?>
   <!-- Endflexible section  -->
   
   <!-- Team member  -->
   <section class="team section background-grey">
      <div class="container">

         <div class="row mt-0 pt-3 pb-3">
            <div class="col-12">
               <h2 class="section-title-smaller text-center color-dark-green"><?php _e( 'Naš tim', 'nsbi' ); ?></h2>
            </div>
         </div>

         <?php
            $args = array(
               'post_type' => 'team',
               'posts_per_page' => -1,
               'post_status' =>'publish'
            );

            $the_query = new WP_Query( $args );

            if ($the_query->have_posts()) : ?>
                  <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                  <!-- Get blog post block  -->
                     <a class="row team_cart background-white mb-4" href="<?php echo the_permalink(get_the_ID()); ?>">
                        <div class="col-12 col-md-6 col-lg-5">
                           <div class="team_image">
                              <?php if( has_post_thumbnail() ): ?>
                                 <div class="aspect-radio-16-9"><?php the_post_thumbnail('medium'); ?></div>
                              <?php endif; ?>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-7 team_content">
                           <h3 class="mb-2 pt-3"><?php the_title(); ?> </h3>
                           <p class="subtitle mb-4"><?php the_field( 'job_title' ); ?></p>
                           <?php the_excerpt(); ?>
                           <span class="btn-link"><?php _e( 'Pročitaj više', 'nsbi' ); ?></span>
                        </div>
                     </a>
                  <?php endwhile; ?>

               <?php 
            endif; 
            wp_reset_postdata();
            ?>
      </div>
   </section>
   <!-- End team member  -->

   <?php if ( have_rows( 'partners' ) ) : ?>
      <section class="section background-white pb-5">
         <div class="container">

            <?php while ( have_rows( 'partners' ) ) : the_row(); ?>

            <div class="row">
               <div class="col-12 d-flex justify-content-center">
                  <h2 class="section-title-smaller section-title-horizontal-line color-dark-green" ><span class="line"><?php the_sub_field( 'section_title' ); ?></span></h2>
               </div>
            </div>

               <?php if ( have_rows( 'partner_logo' ) ) : ?>
                  <div class="row justify-content-center mt-5">
                     <?php while ( have_rows( 'partner_logo' ) ) : the_row(); ?>
                        <?php $logo = get_sub_field( 'logo' ); ?>
                        <?php if ( $logo ) { ?>

                           <a href="<?php the_sub_field( 'site_url' ); ?>" target="_blank" class="col-6 col-md-3" rel="noopener">
                              <div class="partner-logo"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                              </div>
                           </a>
                        <?php } ?>
                        
                     <?php endwhile; ?>
                  <?php else : ?>
                     <?php // no rows found ?>
               <?php endif; ?>
               </div>
            <?php endwhile; ?>
         </div>
      </section>
   <?php endif; ?>

<?php
get_footer();
