<?php /* Template Name: Contact */ get_header(); ?>
      
<div class="contact md-effect-12">
   <div class="row">
      <div class="col-12 col-lg-7 col-xl-6 contact_info">
         
         <div class="row">
            <div class="col-12 col-md-6">
               <img class="contact-logo" src="<?php echo get_template_directory_uri() ?>/img/NSBI-logo-white.svg"
                  alt="NSBI">
               <?php if ( have_rows( 'contact_form', 'option' ) ) : ?>
               <?php while ( have_rows( 'contact_form', 'option' ) ) : the_row(); ?>
               <div class="section-form_header">
                  <?php the_sub_field( 'form_title' ); ?>
               </div>
               <?php endwhile; ?>
               <?php endif; ?>
            </div>
         </div>
         <div class="row">

            <?php if ( have_rows( 'contact_form', 'option' ) ) : ?>
            <?php while ( have_rows( 'contact_form', 'option' ) ) : the_row(); ?>
            <div class="section-form col-12 col-md-6">
               <?php echo do_shortcode(get_sub_field( 'form_shortcode' ) ); ?>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <div class="col-12 col-md-6 contact-info-col">
               <div class="contact_info_box">

                  <?php if ( have_rows( 'contact_info', 'option' ) ) : ?>
                  <?php while ( have_rows( 'contact_info', 'option' ) ) : the_row(); ?>
                  <?php the_sub_field( 'text' ); ?>
                  <p>
                     <a class="contact_info-link" href="tel:<?php the_sub_field( 'mobile' ); ?>"><span class="iconify iconify-icon"
                           data-icon="carbon:phone" data-inline="false"></span> <?php the_sub_field( 'mobile' ); ?></a>
                  </p>
                  <p>
                     <a class="contact_info-link" href="mailto:<?php the_sub_field( 'email' ); ?>"><span class="iconify iconify-icon"
                           data-icon="la:envelope-solid"
                           data-inline="false"></span><?php the_sub_field( 'email' ); ?></a>
                  </p>
                  <?php endwhile; ?>
                  <?php endif; ?>

                  <?php if ( have_rows( 'social_links', 'option' ) ) : ?>
                  <?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>
                  <div class="social-icon">

                     <?php if( get_sub_field( 'facebook' ) ) { ?>
                     <a class="social-icon-link" href="<?php the_sub_field( 'facebook' ); ?>" target="_blank">
                        <div class="icon"><span class="iconify" data-icon="brandico:facebook"
                              data-inline="false"></span></div>
                     </a>
                     <?php
                     }?>

                     <?php if(get_sub_field( 'youtube' )) { ?>
                     <a class="social-icon-link" href="<?php the_sub_field( 'youtube' )?>" target="_blank">
                        <div class="icon"><span class="iconify" data-icon="vaadin:youtube" data-inline="false"></span>
                        </div>
                     </a>
                     <?php
                     }?>

                     <?php if( get_sub_field( 'twitter' )) { ?>
                     <a class="social-icon-link" href="<?php the_sub_field( 'twitter' ) ?>" target="_blank">
                        <div class="icon"><span class="iconify" data-icon="ant-design:twitter-outlined"
                              data-inline="false"></span></div>
                     </a>
                     <?php
                     }?>

                     <?php if( get_sub_field( 'instagram' )) { ?>
                     <a class="social-icon-link" href="<?php the_sub_field( 'instagram' )?>" target="_blank">
                        <div class="icon"><span class="iconify" data-icon="ant-design:instagram-outlined"
                              data-inline="false"></span></div>
                     </a>
                     <?php
                     }?>

                     <?php if( get_sub_field( 'linkedin' )) { ?>
                     <a class="social-icon-link" href="<?php the_sub_field( 'linkedin' )?>" target="_blank">
                        <div class="icon"><span class="iconify" data-icon="il:linkedin"
                              data-inline="false"></span></div>
                     </a>
                     <?php
                     }?>
                     

                  </div>
                  <?php endwhile; ?>
                  <?php endif; ?>
               </div>
            </div>

            <?php if ( have_rows( 'contact_info_columns', 'option' ) ) : ?>
               <?php while ( have_rows( 'contact_info_columns', 'option' ) ) : the_row(); ?>
               <div class="col-12 col-md-6">
               <div class="contact-info-col">
                  <div class="line-left">
                     <h5><?php the_sub_field( 'title' ); ?></h5>
                     <?php the_sub_field( 'text' ); ?>
                  </div>
                  </div>
               </div>
               <?php endwhile; ?>
            <?php else : ?>
               <?php // no rows found ?>
            <?php endif; ?>
         </div>
         
      </div>
      <div class="col-12 col-lg-5 col-xl-6 contact_map">
         <div id="map" class='map'> </div>
      </div>
   </div>
</div>

<?php get_footer(); ?>