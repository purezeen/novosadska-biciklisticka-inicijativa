<?php /* Template Name: Landing page */ get_header('landing'); ?>


<main id="main" class="site-main" role="main">
   <!-- Hero section  -->
   <?php
      if (has_post_thumbnail()) {
      ?>
   <div class="post-hero__wrap">
      <div class="container">
         <div class="post-hero">
            <div class="post-hero__img">
               <?php the_post_thumbnail('1536x1536', array('class' => 'nolazyloaded')); ?>
            </div>
         </div>
      </div>
   </div>
   <?php
      }
     ?>

   <section class="container mt-5 pt-5">
      <h3 class="section-title-smaller color-dark-green text-center mb-3"><?php the_title(); ?></h3>
      <div class="text-center">
         <?php the_content(); ?>
      </div>
   </section>

   <!-- End hero section  -->
   <!-- Get acf flexible section  -->
   <?php get_template_part( 'template-parts/acf/flexible', 'section' ) ?>
   <!-- Endflexible section  -->

<?php
get_footer();