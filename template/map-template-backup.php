<?php  get_header(); ?>

<main id="main" class="site-main" role="main">

   <!-- Map pins  -->
   <?php if ( have_rows( 'add_location_coordinates' ) ) : ?>
      <script>
         /*----------  ACF Map pins, get coordinates and popup information  ----------*/
         var geojson = {
            type: 'FeatureCollection',
            features: [
            <?php while ( have_rows( 'add_location_coordinates' ) ) : the_row(); ?>
               {
               type: 'Feature',
               geometry: {
                  type: 'Point',
                  <?php if ( have_rows( 'enter_latitude_&_longitude' ) ) : ?>
                     <?php while ( have_rows( 'enter_latitude_&_longitude' ) ) : the_row(); ?>
                        coordinates: [<?php the_sub_field( 'longitude' ); ?>, <?php the_sub_field( 'latitude' ); ?>]
                     <?php endwhile; ?>
                  <?php endif; ?>
               
               },
               properties: {
                  <?php if ( have_rows( 'enter_popup_infromation' ) ) : ?>
                     <?php while ( have_rows( 'enter_popup_infromation' ) ) : the_row(); ?>
               title: '<?php the_sub_field( 'title' ); ?>',
               description: '<?php the_sub_field( 'description' ); ?>',
                     
                     <?php endwhile; ?>
                  <?php endif; ?>
                  <?php 
                  $legend_type = get_sub_field( 'choose_legend_type' );
                  if($legend_type == "benzinske_pumpe") {
                     ?>
                  icon: "<?php echo get_template_directory_uri() . '/img/pin1.svg' ?>"
                     <?php
                  }else if($legend_type == "javna_parkiralista") {
                     ?>
                  icon: "<?php echo get_template_directory_uri() . '/img/pin2.svg' ?>"
                     <?php
                  }else if ($legend_type == "lokacije_za_iznajmljivanje_bicikala") {
                     ?>
                  icon: "<?php echo get_template_directory_uri() . '/img/pin3.svg' ?>"
                     <?php
                     }
                  ?>
               }
               },

               <?php endwhile; ?>
            ]
         };
      </script>
   <?php endif; ?>

   <section class="container">
     
      <div class="map-legend-wrap" >
        
              <div id='map-legend'></div>
         
      </div>
      <div class="legend-wrap background-dark">
            <div class="legend-title">
               <h3>Legenda</h3>
            </div>
            <div class="legend-info">
               <div class="legend-icon"><img src="<?php echo get_template_directory_uri()?>/img/line.svg" alt=""></div>
               <p>Biciklističke<br>staze</p>
            </div>
            <div class="legend-info">
               <div class="legend-icon"><img src="<?php echo get_template_directory_uri()?>/img/pin1.svg" alt=""></div>
               <p>Benzinske<br>pumpe</p>
            </div>
            <div class="legend-info">
               <div class="legend-icon"><img src="<?php echo get_template_directory_uri()?>/img/pin2.svg" alt=""></div>
               <p>Javna<br>parkirališta</p>
            </div>
            <div class="legend-info">
               <div class="legend-icon"><img src="<?php echo get_template_directory_uri()?>/img/pin3.svg" alt=""></div>
               <p>Lokacije stanica za iznajmljivanje bicikala</p>
            </div>
         </div>
      
   </section>
   <!-- End map pins  -->

<?php
get_footer();